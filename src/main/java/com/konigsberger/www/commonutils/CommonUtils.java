package com.konigsberger.www.commonutils;

/**
 * Common Utils
 *
 * @author vrapalis
 */
public final class CommonUtils {

    private CommonUtils() {
        throw new IllegalStateException();
    }

    /**
     * Project Version
     */
    public static final String PROJECT_VERSION = "0.0.1";

    /**
     * Greeting
     */
    public void greeting(final String msg) {
        System.out.println(msg);
    }
}
