# COMMON UTILS

---

> Common utility project. Can be used by another projects, goal of
> this project to share common code.

## Gradle

Gradle is used as build tool.

### Publish

```
./gradlew publish -PcommonUtilsRepositoryDeployTokenValue=...
```